//
//  ViewController.swift
//  BisonSDK
//
//  Created by Sirichai Monhom on 01/30/2018.
//  Copyright (c) 2018 Sirichai Monhom. All rights reserved.
//

import UIKit
import BisonSDK
import Alamofire
import UICircularProgressRing
import SnapKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var textview: UITextView!
    @IBAction func selectImage(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var textfield: UITextField!
    
    @IBAction func upload(_ sender: Any) {
        
        self.progressBar.isHidden = false
        self.textview.text = ""
        
        guard let imageSelected  = self.image.image else {
            self.progressBar.isHidden = true
            return
        }
        
        let isOverwrite:Bool = self.switch.isOn
        
        do {
            let bison = try BisonSDK.getInstance()

            let onSuccess:((BisonUploadRespone) -> Void) = { (respone) in
                
                guard let url = respone.url else {
                    return
                }
                //
                self.progressBar.isHidden = true    
                self.textview.isHidden = false
                self.setTextViewWith(url: url)
            }
            
            let onFailure:((Error) -> Void) = { (error) in
                //
                error.alert(on: self)
                self.progressBar.isHidden = true            }
            
            /*
             UploadProgressCallback = ((Progress) -> Void)
             **/
            let onProgress:(UploadProgressCallback) = { uploadProgress  -> Void in
                //
                self.progressBar.setProgress(value: CGFloat(uploadProgress.fractionCompleted)*100, animationDuration: 5, completion: nil)
            }
            
            // convert image to Data with Bison Extention 
            let imageData = try imageSelected.toBisonData(imageQuality: 0.5)
            
            if let fileName = self.textfield.text {
                bison.upload(isReplace: isOverwrite, path: "path", file: imageData, fileName: .specify(fileName), bucketName: "bucket_a", onSuccess: onSuccess, onFailure: onFailure, onProgress: onProgress)
            }
            else {
                bison.upload(isReplace: isOverwrite, path: "path", file: imageData, fileName: .random, bucketName: "bucket_a", onSuccess: onSuccess, onFailure: onFailure, onProgress: onProgress)
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
        
    }
    
    let progressBar = UICircularProgressRingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set progress view
        progressBar.maxValue = 100
        progressBar.value = 0
        progressBar.outerRingWidth = 1
        progressBar.innerRingWidth = 1
        progressBar.innerRingSpacing = 0
        progressBar.innerRingColor = .green
        progressBar.backgroundColor = .white
        self.view.addSubview(progressBar)
        progressBar.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 150, height: 150))
        }
        
        self.progressBar.isHidden = true
        self.textview.isHidden = true
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.image.image = image
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setTextViewWith(url:URL){
        let string = "Upload success : \(url)"
        let attributedString = NSMutableAttributedString(string: string, attributes:[NSAttributedStringKey.link: url])
        
        self.textview.isUserInteractionEnabled = true
        self.textview.attributedText = attributedString
    }
}

extension Error {
    func alert(on:UIViewController) {
        let ctrl = UIAlertController(title: "Error", message: self.localizedDescription, preferredStyle: .alert)
        ctrl.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        on.present(ctrl, animated: true, completion: nil)
    }
}


