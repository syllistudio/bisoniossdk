//
//  BisonRespone.swift
//  Alamofire
//
//  Created by Sirichai Monhom on 2/1/18.
//

import Foundation
import ObjectMapper
import UIKit


public class BisonRespone: NSObject, Mappable {
    
    var isSuccess:Bool = false
    var message:String = ""
    
    override init() {
        super.init()
    }
    
    required public init?(map: Map) {
        super.init()
    }
    
    public func mapping(map: Map) {
        // Perform Mapping
        self.isSuccess      <- map["status"]
        self.message        <- map["message"]
    }
    
    public func getError() -> Error?  {
        if isSuccess {
            return nil
        }
        // Otherwise build error object from instances' message.
        return BisonSDKError.default(message: "\(self.message)")
    }
}

public class BisonUploadRespone: BisonRespone {
    
    public var bucketName:String = ""
    public var path:String = ""
    public var url:URL? = nil
    public var isOverwrite:Bool = false
    
    override init() {
        super.init()
    }
    
    required public init?(map: Map) {
        super.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        // Perform Mapping
        self.bucketName     <- map["bucket"]
        self.path           <- map["path"]
        self.url            <- (map["url"],URLTransform())
        self.isOverwrite    <- map["query.overwrite"]
    }
}
