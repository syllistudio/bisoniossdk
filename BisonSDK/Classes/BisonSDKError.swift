//
//  BisonSDKError.swift
//  Alamofire
//
//  Created by Sirichai Monhom on 2/5/18.
//

import Foundation


public enum BisonSDKError : LocalizedError {
    
    /**
     default error case, where there is no specificiation to support such error case.
     
     Should report to User - No
     */
    case `default`(message:String)

    /**
     Error case where SDK Consumer provided bad configuration file. Or there is no configuration file.
     */
    case badConfigurationFile(name:String)
    
    /**
     Error case where SDK Can't get configuration some field.
     */
    case invalidConfiguration(field: String, fileName: String)
    
    /**
     
     */
    case unknowError

    public var errorDescription: String? {
        switch self {
        case .default(let message):
            return "BisonSDKError: \(message)"
        case .badConfigurationFile(let name):
            return "BisonSDKError: Unknown configuration filename: \(name)"
        case .invalidConfiguration(let field, let fileName):
            return "BisonSDK: InvalidConfiguration \(field) from \(fileName) doesn't exists."
        default:
            return "BisonSDKError: Unknow."
        }
    }
    
}

public extension Error {
    public var description: String {
        guard let apiError = self as? BisonSDKError else { return "BisonSDKError : Unknow" }
        return apiError.errorDescription ?? "BisonSDKError : Unknow"
    }
}

