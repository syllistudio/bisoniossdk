//
//  BisonSDK.swift
//  BisonSDK
//
//  Created by Sirichai Monhom on 1/31/18.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public let Bison_config_default = "config"

public typealias UploadProgressCallback = ((Progress) -> Void)

public enum FileNameOptions {
    case specify(String)
    case random
    
    func getResourceURL(domain:String, bucketName:String, path:String, isReplace:Bool) -> String {
        
        //
        var domainUrl = ""
        
        // Move this to ENUM's function
        switch self {
        case .random:
            domainUrl = domain + bucketName + "/" + path + "/"
        case .specify(let fileName):
            domainUrl = domain + bucketName + "/" + path + "/" + fileName
        }
        if !isReplace {
            domainUrl = domain + "?overwrite=false"
        }
        
        return domainUrl
    }
}

public extension UIImage {
    func toBisonData(imageQuality:CGFloat) throws -> Data {
        guard let data:Data = UIImageJPEGRepresentation(self, imageQuality) else {
            throw BisonSDKError.default(message: "Convert UIImage to Data failed.")
        }
        return data
    }
}

public class BisonSDK: NSObject {
    
    // Get value from json config When Call getInstance().
    public var client_id: String
    public var client_secret: String
    public var domain: String
    
    // Singleton
    private static var instanceDic = [String : BisonSDK]()
    
    private init(configName:String) throws {
        
        // Actual Filename
        let fileName = "bison-" + configName
        
        // Load configuration from JSON file
        guard let pathURL = Bundle.main.url(forResource: fileName, withExtension: "json") else {
            throw BisonSDKError.badConfigurationFile(name: fileName)
        }
        
        do {
            let jsonData = try Data(contentsOf: pathURL, options: .mappedIfSafe)
            
            let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! [String: Any]
            
            // Validate configurations
            guard let domain = jsonResult["domain"] as? String else {
                throw BisonSDKError.invalidConfiguration(field: "Domain", fileName: fileName)
            }
            
            guard let clientSecret = jsonResult["client_secret"] as? String else {
                throw BisonSDKError.invalidConfiguration(field: "client_secret", fileName: fileName)
            }
            
            guard let clientId = jsonResult["client_id"] as? String else {
                throw BisonSDKError.invalidConfiguration(field: "client_id", fileName: fileName)
            }
            
            self.client_id = clientId
            self.client_secret = clientSecret
            self.domain = domain
        }
        catch(let error) {
            throw BisonSDKError.default(message:"\(error.localizedDescription)")
        }
        
        super.init()
    }
    
    /**
     * Gets a bison using the default config file.
     *
     * @param configName - External config file name.
     * @return BisonSDK
     */
    public static func getInstance(configName:String = Bison_config_default) throws -> BisonSDK   {
        
        // In case instance already exists.
        if let instance = self.instanceDic[configName] {
            return instance
        }
        
        let instance = try BisonSDK.init(configName: configName)
        self.instanceDic[configName] = instance
        
        return instance
    }
    
    /**
     * @param isReplace - true if the file will be overwritten on server.
     *           false if the file will not be overwritten on server.
     * @param path - Directory path that you want to prepareUpload file.
     * @param file - File that you want to prepare upload.
     * Upload file with 'FileNameOption' is ENUM which support .specify(<string>) or .random file name generate by Bison storage server
     * @param bucketName - Bison's bucket name.
     * @param onSuccess - Callback function will be called when the HTTP response was successfully returned by the remote server.
     * @param onFailure - Callback function will be called when the request could not be executed due to cancellation, a connectivity problem or timeout.
     * @param onProgress - Callback function will be called when the file is uploading.
     */
    public func upload(isReplace:Bool = true,
                path:String,
                file:Data,
                fileName:FileNameOptions,
                bucketName:String,
                onSuccess: @escaping (BisonUploadRespone)->(),
                onFailure: @escaping (Error)->(),
                onProgress: UploadProgressCallback?) -> Void {
        
        let bisonHeaders: HTTPHeaders = [
            "Content-type":  "application/json",
            "Authorization": "\(client_id) \(client_secret)"
        ]
        
        let domain = fileName.getResourceURL(domain: self.domain, bucketName: bucketName, path: path, isReplace: isReplace)
        
        Alamofire.upload (
            multipartFormData: { multipartFormData in
                multipartFormData.append(file, withName: "FILE", fileName: "image.jpg", mimeType: "image/jpg")
            },
            to: domain,
            method:.post,
            headers:bisonHeaders,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    // onProgress case
                    if let progressHandler = onProgress {
                        upload.uploadProgress(closure: progressHandler)
                    }
                    
                    // onSuccess case
                    upload.responseObject(completionHandler: { (response : DataResponse<BisonUploadRespone>) in
                        
                        guard let value = response.result.value else {
                            onFailure(BisonSDKError.default(message: "BisonSDK Upload Success but Value is nil."))
                            return
                        }
                        
                        if value.isSuccess {
                            onSuccess(value)
                        }
                        else {
                            NSLog("BisonSDK Upload Failure Status:  \(String(describing: response.response?.statusCode)).")
                            
                            if let error = response.error {
                                onFailure(error)
                                return
                            }
                            
                            if let error = response.value?.getError() {
                                onFailure(error)
                                return
                            }
                            
                            onFailure(BisonSDKError.default(message: "BisonSDK Upload Failure but respone is nil."))
                            return
                        }
                    })
                    
                case .failure(let encodingError):
                    onFailure(encodingError)
                }
            }
        )
    }
}
