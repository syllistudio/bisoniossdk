# BisonSDK

[![CI Status](http://img.shields.io/travis/Sirichai Monhom/BisonSDK.svg?style=flat)](https://travis-ci.org/Sirichai Monhom/BisonSDK)
[![Version](https://img.shields.io/cocoapods/v/BisonSDK.svg?style=flat)](http://cocoapods.org/pods/BisonSDK)
[![License](https://img.shields.io/cocoapods/l/BisonSDK.svg?style=flat)](http://cocoapods.org/pods/BisonSDK)
[![Platform](https://img.shields.io/cocoapods/p/BisonSDK.svg?style=flat)](http://cocoapods.org/pods/BisonSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BisonSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BisonSDK'
```

## Author

Sirichai Monhom, sirichai@syllistudio.com

## License

BisonSDK is available under the MIT license. See the LICENSE file for more info.
